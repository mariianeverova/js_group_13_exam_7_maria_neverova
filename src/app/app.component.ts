import { Component } from '@angular/core';
import {Item} from "./shared/items.module";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  itemsArray: Item[] = [
    new Item ('Hamburger', 80, 0, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTMPV6vw2NCAVbNqoze7cobY-ApS05ZTPWAkNo8zvUx5RyU9lpQhmyTed9NJZ5xPNS_-c&usqp=CAU'),
    new Item ('Cheeseburger', 90, 0, 'https://icon-library.com/images/cheeseburger-icon/cheeseburger-icon-2.jpg'),
    new Item ('Fries', 45, 0, 'https://raw.githubusercontent.com/solana-labs/token-list/main/assets/mainnet/FriCEbw1V99GwrJRXPnSQ6su2TabHabNxiZ3VNsVFPPN/logo.png'),
    new Item ('Coffee', 70, 0, 'https://icons-for-free.com/iconfiles/png/512/Coffee-1320568040449299331.png'),
    new Item ('Tea', 50, 0, 'https://cdn.iconscout.com/icon/free/png-256/tea-mug-beverage-kitchen-food-drink-coffee-cup-1-30183.png'),
    new Item ('Cola', 40, 0, 'https://www.iconninja.com/files/487/838/510/soda-can-drink-cola-coke-beverage-packaging-icon.png')
  ]

  itemsOrder: Item[] = [];

  addItemToCart(i: number) {
    this.itemsArray[i].amount++;
    if (this.itemsOrder.length > 0) {
      let addedItem = this.itemsOrder.includes(this.itemsArray[i]);
      if (addedItem) {
        return;
      } else {
        this.itemsOrder.push(this.itemsArray[i]);
      }
    } else {
      this.itemsOrder.push(this.itemsArray[i]);
    }
  }

  deleteItemFromCart(i: number) {
    this.itemsOrder[i].amount = 0;
    this.itemsOrder.splice(i, 1);
  }
}
