import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../../shared/items.module";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {
  @Input() item!: Item;
  @Output() addItem = new EventEmitter();

  onAddItem() {
    this.addItem.emit();
    console.log('added');
  }
}
