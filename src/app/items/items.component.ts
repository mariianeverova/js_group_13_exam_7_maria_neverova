import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/items.module";

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent {
  @Input() items!: Item[];
  @Output() addedItem = new EventEmitter();

  clickOnItem(i: number) {
    this.addedItem.emit(i);
  }
}
