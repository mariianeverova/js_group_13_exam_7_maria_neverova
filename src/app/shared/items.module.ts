export class Item {
  constructor(
    public name: string,
    public price: number,
    public amount: number,
    public image: string,
  ) {}

  getTotalPrice() {
    return this.price * this.amount;
  }
}
