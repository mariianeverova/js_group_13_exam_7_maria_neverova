import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../../shared/items.module";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent {
  @Input() items!: Item;
  @Output() deleteItem = new EventEmitter();

  onDeleteItem() {
    this.deleteItem.emit();
  }
}

