import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/items.module";

@Component({
  selector: 'app-order-block',
  templateUrl: './order-block.component.html',
  styleUrls: ['./order-block.component.css']
})
export class OrderBlockComponent {
  @Input() items! : Item[];
  @Output() deleteItem = new EventEmitter();
  orderIsEmpty = true;

  orderEmpty() {
    if (this.items.length !== 0) {
      this.orderIsEmpty = false;
    } else if (this.items.length === 0) {
      this.orderIsEmpty = true;
    }
    return this.orderIsEmpty;
  }

  onDeleteItem(i: number) {
    this.deleteItem.emit(i);
  }

  getTotalSum() {
    let totalSum = 0;
    this.items.forEach(item => {
      totalSum = totalSum + item.getTotalPrice();
    })
    return totalSum;
  }
}
