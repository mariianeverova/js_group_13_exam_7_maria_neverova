import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './items/item/item.component';
import { OrderBlockComponent } from './order-block/order-block.component';
import { OrdersComponent } from './order-block/orders/orders.component';

@NgModule({
    declarations: [
        AppComponent,
        ItemsComponent,
        ItemComponent,
        OrderBlockComponent,
        OrdersComponent,
        OrdersComponent
    ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
